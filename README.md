# Travix Frontend Test

Minimum Requirements:
* NodeJS 10.9
* Yarn 1.9.4

## Instructions

The application was split into two to keep them decoupled by Single Responsability Principle. This will allow to maintain the application easily and escalate them quickly.

In order to use the application you must start both applications, the server and the client individually.

### Service

Access the `service` folder

* `$ yarn`
* `$ yarn start`

### Client

Access the `client` folder

* `$ yarn`
* `$ yarn start`
* `yarn start`
* Open application in the browser by going into: `localhost:3000`
