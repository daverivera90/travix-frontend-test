// @vendors
import thunk from 'redux-thunk'
import { createStore, applyMiddleware, compose } from 'redux'

// @components
import rootReducer from './rootReducer'

const middleware = [thunk]
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middleware))
)

export default store
