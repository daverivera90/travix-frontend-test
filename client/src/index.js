// @vendors
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

// @components
import './assets/fonts'
import registerServiceWorker from './registerServiceWorker'
import TodoManager from './todo-manager'
import store from './store'
import './utils/axios-defaults'

// @styles
import './index.scss'

ReactDOM.render(
    <Provider store={store}>
        <TodoManager />
    </Provider>, document.getElementById('root')
)
registerServiceWorker()
