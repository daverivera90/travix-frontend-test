export const ENDPOINTS = {
    createTask: '/task/create',
    deleteTask: '/task/delete',
    getTasksList: '/tasks',
    updateTask: '/task/update'
}
