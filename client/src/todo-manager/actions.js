// @vendors
import axios from 'axios'

// @comonents
import { ENDPOINTS } from '../utils/constants'

export const DIALOG_EDIOR_TOGGLE_DISPLAY = 'DIALOG_EDIOR_TOGGLE_DISPLAY'
export const displayDialogEditor = (isEditMode = false) =>
    ({
        type: DIALOG_EDIOR_TOGGLE_DISPLAY,
        payload: {
            isEditMode,
            isVisible: true
        }
    })

export const hideDialogEditor = () =>
    ({
        type: DIALOG_EDIOR_TOGGLE_DISPLAY,
        payload: {
            isEditMode: false,
            isVisible: false
        }
    })

export const SET_TASK_TITLE = 'SET_TASK_TITLE'
export const updateTitle = newTitle =>
    ({
        type: SET_TASK_TITLE,
        payload: newTitle
    })

export const SET_TASK_DESCRIPTION = 'SET_TASK_DESCRIPTION'
export const updateDescription = newDescription =>
    ({
        type: SET_TASK_DESCRIPTION,
        payload: newDescription
    })

export const GET_TASKS_LIST_REQUEST = 'GET_TASKS_LIST_REQUEST'
export const GET_TASKS_LIST_SUCCESS = 'GET_TASKS_LIST_SUCCESS'
export const getTasksList = () =>
    (dispatch) => {
        dispatch({ type: GET_TASKS_LIST_REQUEST })
        return axios
            .get(ENDPOINTS.getTasksList)
            .then(({ data }) => {
                dispatch({
                    type: GET_TASKS_LIST_SUCCESS,
                    payload: data.tasks
                })
            })
    }

export const CREATE_TASK_REQUEST = 'CREATE_TASK_REQUEST'
export const CREATE_TASK_SUCCESS = 'CREATE_TASK_SUCCESS'
export const createTask = ({ description, title }) =>
    (dispatch) => {
        dispatch({ type: CREATE_TASK_REQUEST })
        return axios
            .post(`${ENDPOINTS.createTask}/${title}/${description}`)
            .then(() => {
                dispatch({
                    type: CREATE_TASK_SUCCESS,
                    payload: { description, title }
                })
            })
    }

export const SET_ACTIVE_TASK = 'SET_ACTIVE_TASK'
export const setActiveTask = taskId =>
    (dispatch, getState) => {
        const activeTask = getState()
            .tasks
            .getIn(['tasks', 'list'])
            .find(task => task.get('id') === taskId)

        dispatch({
            type: SET_ACTIVE_TASK,
            payload: activeTask
        })
    }

export const UPDATE_ACTIVE_TASK_REQUEST = 'UPDATE_ACTIVE_TASK_REQUEST'
export const UPDATE_ACTIVE_TASK_SUCCESS = 'UPDATE_ACTIVE_TASK_SUCCESS'
export const updateActiveTask = () =>
    (dispatch, getState) => {
        dispatch({ type: UPDATE_ACTIVE_TASK_REQUEST })

        const tasksState = getState().tasks
        const { description, id, title } = tasksState.get('activeTask').toJS()
        const tasksList = tasksState.getIn(['tasks', 'list'])

        return axios
            .put(`${ENDPOINTS.updateTask}/${id}/${title}/${description}`)
            .then(() => {
                const updatedTask = tasksList.update(
                    tasksList.findIndex(task => task.get('id') === id),
                    task => task.merge({ description, id, title })
                )

                dispatch({
                    type: UPDATE_ACTIVE_TASK_SUCCESS,
                    payload: updatedTask
                })
            })
    }

export const DELETE_TASK_REQUEST = 'DELETE_TASK_REQUEST'
export const DELETE_TASK_SUCESS = 'DELETE_TASK_SUCESS'
export const deleteTask = taskId =>
    (dispatch) => {
        dispatch({ type: DELETE_TASK_REQUEST })

        return axios
            .delete(`${ENDPOINTS.deleteTask}/${taskId}`)
            .then(() => {
                dispatch({ type: DELETE_TASK_SUCESS })
            })
    }
