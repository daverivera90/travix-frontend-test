// @vendors
import { fromJS, List } from 'immutable'

// @components
import {
    CREATE_TASK_REQUEST,
    CREATE_TASK_SUCCESS,
    DIALOG_EDIOR_TOGGLE_DISPLAY,
    GET_TASKS_LIST_REQUEST,
    GET_TASKS_LIST_SUCCESS,
    SET_ACTIVE_TASK,
    SET_TASK_DESCRIPTION,
    SET_TASK_TITLE,
    UPDATE_ACTIVE_TASK_REQUEST,
    UPDATE_ACTIVE_TASK_SUCCESS
} from './actions'

const initialState = fromJS({
    editorConfig: {
        isEditMode: false,
        isVisible: false
    },
    isStoringTask: false,
    activeTask: {
        description: '',
        id: undefined,
        title: ''
    },
    tasks: {
        isLoading: false,
        list: []
    }
})
export const tasksReducer = (state = initialState, action) => {
    switch(action.type) {
        case CREATE_TASK_REQUEST:
        case UPDATE_ACTIVE_TASK_REQUEST:
            return state.set('isStoringTask', true)
        case UPDATE_ACTIVE_TASK_SUCCESS:
        case CREATE_TASK_SUCCESS:
            return state.merge({
                activeTask: {
                    description: '',
                    id: undefined,
                    title: ''
                },
                isStoringTask: false
            })
        case DIALOG_EDIOR_TOGGLE_DISPLAY:
            return state.merge({
                editorConfig: { ...action.payload }
            })
        case GET_TASKS_LIST_REQUEST:
            return state.setIn(['tasks', 'isLoading'], true)
        case GET_TASKS_LIST_SUCCESS:
            return state.set('tasks', fromJS({
                isLoading: false,
                list: action.payload
            }))
        case SET_ACTIVE_TASK:
            return state.set('activeTask', action.payload)
        case SET_TASK_DESCRIPTION:
            return state.setIn(['activeTask', 'description'], action.payload)
        case SET_TASK_TITLE:
            return state.setIn(['activeTask', 'title'], action.payload)
        default:
            return state
    }
}
