// @vendors
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// @components
import TaskEditor from '../task-editor'
import {
    createTask,
    getTasksList,
    hideDialogEditor,
    updateActiveTask,
    updateDescription,
    updateTitle
} from '../../actions'

const mapStateToProps = ({ tasks }) => ({
    activeTask: tasks.get('activeTask'),
    editorConfig: tasks.get('editorConfig'),
    isStoringTask: tasks.get('isStoringTask')
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createTask,
    getTasksList,
    hideDialogEditor,
    updateActiveTask,
    updateDescription,
    updateTitle
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TaskEditor)
