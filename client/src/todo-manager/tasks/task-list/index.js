// @vendors
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
    Button,
    CircularProgress,
    List,
    ListItem
} from 'react-md'

// @styles
import './style.scss';

class TasksList extends PureComponent {
    constructor(props) {
        super(props)

        this.renderTasks = this.renderTasks.bind(this)
    }

    renderTasks() {
        const {
            deleteTask,
            openEditorDialog,
            tasks
        } = this.props

        if (!tasks.length) {
            return [
                <ListItem
                    className="tasks-list__item tasks-list__empty-list"
                    key="0"
                    primaryText="There are no tasks created yet. Write down a title in the field above and press the 'Enter' key"
                >
                </ListItem>
            ]
        }

        return tasks.map((task, index) => (
            <ListItem
                className="tasks-list__item"
                key={index}
                primaryText={task.title}
                renderChildrenOutside
                secondaryText={task.description}
            >
                <Button
                    className="tasks-list__delete-button"
                    icon
                    onClick={() => deleteTask(task.id)}
                    primary
                >
                    delete
                </Button>
                <Button
                    className="tasks-list__edit-button"
                    icon
                    onClick={() => openEditorDialog(task.id)}
                    primary
                >
                    edit
                </Button>
            </ListItem>
        ))
    }

    render() {
        const { isLoading } = this.props

        if (isLoading) {
            return <CircularProgress id="task-list-loading" />
        }


        return (
            <List>
                {this.renderTasks()}
            </List>
        )
    }
}

TasksList.propTypes = {
    deleteTask: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    openEditorDialog: PropTypes.func.isRequired,
    tasks: PropTypes.arrayOf(
        PropTypes.shape({
            description: PropTypes.string.isRequired,
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired
        })
    ).isRequired
}

export default TasksList
