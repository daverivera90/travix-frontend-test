// @vendors
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'immutable-prop-types'
import {
    Divider,
    Paper,
    TextField
} from 'react-md'

// @components
import TaskEditor from './containers/task-editor-container'
import TaskList from './task-list'

// @styles
import './style.scss'

const ENTER_KEY = 'Enter'

class Tasks extends PureComponent {
    constructor(props) {
        super(props)

        this.handleKeyPress = this.handleKeyPress.bind(this)
        this.onDeleteTask = this.onDeleteTask.bind(this)
        this.openEditorDialog = this.openEditorDialog.bind(this)
    }

    componentDidMount() {
        this.props.getTasksList()
    }

    handleKeyPress(e) {
        const { displayDialogEditor, newTaskTitle } = this.props

        const isEnterKey = e.key === ENTER_KEY
        const isNotEmptyTitle = !!newTaskTitle

        if (isEnterKey && isNotEmptyTitle) {
            displayDialogEditor()
        }
    }

    openEditorDialog(taskId) {
        const { displayDialogEditor, setActiveTask } = this.props

        displayDialogEditor(true)
        setActiveTask(taskId)
    }

    onDeleteTask(taskId) {
        const { deleteTask, getTasksList } = this.props

        deleteTask(taskId).then(() => getTasksList())
    }

    render() {
        const {
            newTaskTitle,
            tasks,
            updateTitle
        } = this.props

        return [
            <TaskEditor key="1"/>,
            <Paper key="2" className="tasks">
                <TextField
                    className="tasks__title-input"
                    id="task-title"
                    label="Title"
                    lineDirection="center"
                    onChange={updateTitle}
                    value={newTaskTitle}
                    onKeyPress={this.handleKeyPress}
                    placeholder="New task title"
                />
                <Divider />
                <TaskList
                    openEditorDialog={this.openEditorDialog}
                    isLoading={tasks.get('isLoading')}
                    tasks={tasks.get('list').toJS()}
                    deleteTask={this.onDeleteTask}
                />
            </Paper>
        ]
    }
}

Tasks.propTypes = {
    deleteTask: PropTypes.func.isRequired,
    displayDialogEditor: PropTypes.func.isRequired,
    getTasksList: PropTypes.func.isRequired,
    newTaskTitle: PropTypes.string.isRequired,
    setActiveTask: PropTypes.func.isRequired,
    tasks: ImmutablePropTypes.mapContains({
        isLoading: PropTypes.bool.isRequired,
        list: ImmutablePropTypes.list.isRequired
    }).isRequired,
    updateTitle: PropTypes.func.isRequired
}

export default Tasks
