// @vendors
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'immutable-prop-types'
import classNames from 'classnames'
import {
    Button,
    CircularProgress,
    DialogContainer,
    TextField
} from 'react-md'

// @styles
import './style.scss'

class TaskEditor extends PureComponent {
    constructor(props) {
        super(props)

        this.onSubmitForm = this.onSubmitForm.bind(this)
    }

    onSubmitForm() {
        const {
            activeTask,
            createTask,
            editorConfig,
            getTasksList,
            hideDialogEditor,
            updateActiveTask
        } = this.props

        const requestPromise = editorConfig.get('isEditMode')
            ? updateActiveTask()
            : createTask({
                description: activeTask.get('description'),
                title: activeTask.get('title')
            })

        requestPromise
            .then(() => hideDialogEditor())
            .then(() => getTasksList())

    }

    renderSubmitButton() {
        const { activeTask, isStoringTask } = this.props

        if(isStoringTask) {
            return (
                <div className="task-editor__loading-indicator">
                    <CircularProgress id="submit-task-loading"/>
                </div>
            )
        }

        const isButtonDisabled = !activeTask.get('title') || !activeTask.get('description')
        return (
            <Button
                className={classNames(
                    'task-editor__submit-button',
                    { 'task-editor__submit-button--disabled': isButtonDisabled }
                )}
                flat
                primary
                swapTheming
                disabled={isButtonDisabled}
                onClick={this.onSubmitForm}
            >
                Submit
            </Button>
        )
    }

    render() {
        const {
            hideDialogEditor,
            editorConfig,
            activeTask,
            updateDescription,
            updateTitle
        } = this.props

        return (
            <DialogContainer
                id="simple-list-dialog"
                visible={editorConfig.get('isVisible')}
                title="Create a new task"
                onHide={hideDialogEditor}
            >
                <TextField
                    id="task-editor-title"
                    label="Title"
                    lineDirection="center"
                    onChange={updateTitle}
                    value={activeTask.get('title')}
                />
                <TextField
                    id="task-editor-description"
                    label="Description"
                    lineDirection="center"
                    onChange={updateDescription}
                    value={activeTask.get('description')}
                />
                <div className="task-editor__button-group">
                    <Button
                        flat
                        swapTheming
                        className="task-editor__cancel-button"
                        onClick={hideDialogEditor}
                    >
                        Cancel
                    </Button>
                    {this.renderSubmitButton()}
                </div>
            </DialogContainer>
        )
    }
}

TaskEditor.propTypes = {
    activeTask: ImmutablePropTypes.mapContains({
        description: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    }).isRequired,
    createTask: PropTypes.func.isRequired,
    editorConfig: ImmutablePropTypes.mapContains({
        isEditMode: PropTypes.bool.isRequired,
        isVisible: PropTypes.bool.isRequired
    }),
    getTasksList: PropTypes.func.isRequired,
    hideDialogEditor: PropTypes.func.isRequired,
    isStoringTask: PropTypes.bool.isRequired,
    updateActiveTask: PropTypes.func.isRequired,
    updateDescription: PropTypes.func.isRequired,
    updateTitle: PropTypes.func.isRequired
}

export default TaskEditor
