// @vendors
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// @components
import Tasks from '../tasks'
import {
    deleteTask,
    displayDialogEditor,
    getTasksList,
    setActiveTask,
    updateTitle
} from '../actions'

const mapStateToProps = ({ tasks }) => ({
    newTaskTitle: tasks.getIn(['activeTask', 'title']),
    tasks: tasks.get('tasks')
})

const mapDispatchToProps = dispatch => bindActionCreators({
    deleteTask,
    displayDialogEditor,
    getTasksList,
    setActiveTask,
    updateTitle
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Tasks)
