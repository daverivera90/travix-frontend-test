// @vendors
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { fromJS } from 'immutable'

// @components
import { ENDPOINTS } from '../../utils/constants'
import {
    CREATE_TASK_REQUEST,
    CREATE_TASK_SUCCESS,
    DELETE_TASK_REQUEST,
    DELETE_TASK_SUCESS,
    DIALOG_EDIOR_TOGGLE_DISPLAY,
    GET_TASKS_LIST_REQUEST,
    GET_TASKS_LIST_SUCCESS,
    SET_ACTIVE_TASK,
    SET_TASK_DESCRIPTION,
    SET_TASK_TITLE,
    UPDATE_ACTIVE_TASK_REQUEST,
    UPDATE_ACTIVE_TASK_SUCCESS
} from '../actions'
import {
    createTask,
    deleteTask,
    displayDialogEditor,
    getTasksList,
    hideDialogEditor,
    setActiveTask,
    updateActiveTask,
    updateDescription,
    updateTitle
} from '../actions'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Display Editor Dialog', () => {
    context('when showing the dialog', () => {
        let dispatchedAction;

        beforeAll(() => {
            dispatchedAction = displayDialogEditor()
        })

        it('should dispatch DIALOG_EDIOR_TOGGLE_DISPLAY action', () => {
            expect(dispatchedAction.type).toEqual(DIALOG_EDIOR_TOGGLE_DISPLAY)
        })

        it('should contain the editor configuration as payload', () => {
            expect(dispatchedAction.payload).toEqual({
                isEditMode: false,
                isVisible: true
            });
        })
    })

    context('when hiding the dialog', () => {
        let dispatchedAction

        beforeAll(() => {
            dispatchedAction = hideDialogEditor()
        })

        it('should dispatch DIALOG_EDIOR_TOGGLE_DISPLAY action', () => {
            expect(dispatchedAction.type).toEqual(DIALOG_EDIOR_TOGGLE_DISPLAY)
        })

        it('should contain the editor configutation as payload', () => {
            expect(dispatchedAction.payload).toEqual({
                isEditMode: false,
                isVisible: false
            });
        })
    })
})

describe('New Task Details', () => {
    context('when updating the task title', () => {
        const NEW_TASK_TITLE = 'Task Title'
        let dispatchedAction

        beforeAll(() => {
            dispatchedAction = updateTitle(NEW_TASK_TITLE)
        })

        it('should dispatch SET_TASK_TITLE action', () => {
            expect(dispatchedAction.type).toEqual(SET_TASK_TITLE)
        })

        it('should contain the new title value', () => {
            expect(dispatchedAction.payload).toEqual(NEW_TASK_TITLE)
        })
    })

    context('when updating the task description', () => {
        const NEW_TASK_DESCRIPTION = 'Description'
        let dispatchedAction

        beforeAll(() => {
            dispatchedAction = updateDescription(NEW_TASK_DESCRIPTION)
        })

        it('should dispatch SET_TASK_TITLE action', () => {
            expect(dispatchedAction.type).toEqual(SET_TASK_DESCRIPTION)
        })

        it('should contain the new title value', () => {
            expect(dispatchedAction.payload).toEqual(NEW_TASK_DESCRIPTION)
        })
    })
})

describe('Fetch Tasks List', () => {
    context('when fetching the tasks list', () => {
        it('should create an action with the tasks list as payload', () => {
            const TASKS_LIST = fromJS([{
                title: 'Watchmen',
                description: 'Best comic ever!'
            }])
            const axiosMock = new MockAdapter(axios)
            axiosMock
                .onGet(ENDPOINTS.getTasksList)
                .reply(200, { tasks: TASKS_LIST.toJS() })

            const expectedActions = [
                { type: GET_TASKS_LIST_REQUEST },
                {
                    type: GET_TASKS_LIST_SUCCESS,
                    payload: TASKS_LIST.toJS()
                }
            ]

            const store = mockStore()
            return store.dispatch(getTasksList()).then(() => {
                const dispatchedActions = store.getActions()

                expect(dispatchedActions).toHaveLength(2)
                dispatchedActions.forEach(action => expect(expectedActions).toContainEqual(action))
            })
        })
    })
})

describe('Create New Tasks', () => {
    context('when creating a new task', () => {
        it('should add the task to the list', () => {
            const NEW_TASK = { title: 'Watchmen', description: 'Best comic ever!' }

            const axiosMock = new MockAdapter(axios)
            axiosMock
                .onPost(`${ENDPOINTS.createTask}/Watchmen/Best comic ever!`)
                .reply(201, 'Resource created')

            const expectedActions = [
                { type: CREATE_TASK_REQUEST },
                {
                    type: CREATE_TASK_SUCCESS,
                    payload: NEW_TASK
                }
            ]

            const store = mockStore()
            return store.dispatch(createTask(NEW_TASK)).then(() => {
                const dispatchedActions = store.getActions()

                expect(dispatchedActions).toHaveLength(2)
                dispatchedActions.forEach(action => expect(expectedActions).toContainEqual(action))
            })
        })
    })
})

describe('Edit Task', () => {
    context('when selecting a task to edit', () => {
        it('should find the task from the tasks list', () => {
            const SELECTED_TASK_ID = 123
            const initialState = {
                tasks: fromJS({
                    tasks: {
                        list: [{
                            description: 'Fear is the mind killer',
                            id: 123,
                            title: 'Dune'
                        }, {
                            description: 'The city is afraid of me',
                            id: 231,
                            title: 'Watchmen'
                        }]
                    }
                })
            }
            const store = mockStore(initialState)
            store.dispatch(setActiveTask(SELECTED_TASK_ID))
            const dispatchedActions = store.getActions()

            expect(dispatchedActions).toHaveLength(1)
            expect(dispatchedActions[0]).toEqual({
                type: SET_ACTIVE_TASK,
                payload: fromJS({
                    description: 'Fear is the mind killer',
                    id: 123,
                    title: 'Dune'
                })
            })
        })
    })

    context('when updating the active task', () => {
        it('should create an action to update the task from the list', () => {
            const initialState = {
                tasks: fromJS({
                    activeTask: {
                        description: 'Best comic ever',
                        id: 123,
                        title: 'Watchmen'
                    },
                    tasks: {
                        list: [{
                            description: 'Best',
                            id: 123,
                            title: 'Watchmen'
                        }]
                    }
                })
            }
            const axiosMock = new MockAdapter(axios)
            axiosMock
                .onPut('/task/update/123/Watchmen/Best comic ever')
                .reply(204)

            const expectedActions = [
                { type: UPDATE_ACTIVE_TASK_REQUEST },
                {
                    type: UPDATE_ACTIVE_TASK_SUCCESS,
                    payload: fromJS([{
                        description: 'Best comic ever',
                        id: 123,
                        title: 'Watchmen'
                    }])
                }
            ]

            const store = mockStore(initialState)
            return store.dispatch(updateActiveTask()).then(() => {
                const dispatchedActions = store.getActions()

                expect(dispatchedActions).toHaveLength(2)
                dispatchedActions.forEach(action => expect(expectedActions).toContainEqual(action))
            })
        })
    })
})

describe('Delete tasks', () => {
    context('when deleting a task', () => {
        it('should create an action to remove it from the list', () => {
            const TASK_ID = 123

            const axiosMock = new MockAdapter(axios)
            axiosMock
                .onDelete('/task/delete/123')
                .reply(200)

            const expectedActions = [
                { type: DELETE_TASK_REQUEST },
                { type: DELETE_TASK_SUCESS }
            ]

            const store = mockStore()
            return store.dispatch(deleteTask(TASK_ID)).then(() => {
                const dispatchedActions = store.getActions()

                expect(dispatchedActions).toHaveLength(2)
                dispatchedActions.forEach(action => expect(expectedActions).toContainEqual(action))
            })
        })
    })
})
