// @vendors
import { fromJS } from 'immutable'

// @components
import { tasksReducer } from '../reducer'
import {
    CREATE_TASK_REQUEST,
    CREATE_TASK_SUCCESS,
    DIALOG_EDIOR_TOGGLE_DISPLAY,
    GET_TASKS_LIST_REQUEST,
    GET_TASKS_LIST_SUCCESS,
    SET_ACTIVE_TASK,
    SET_TASK_DESCRIPTION,
    SET_TASK_TITLE,
    UPDATE_ACTIVE_TASK_REQUEST,
    UPDATE_ACTIVE_TASK_SUCCESS
} from '../actions'

describe('Initial State', () => {
    context('when initializing the reducer', () => {
        const expectedInitialState = fromJS({
            editorConfig: {
                isEditMode: false,
                isVisible: false
            },
            isStoringTask: false,
            activeTask: {
                description: '',
                id: undefined,
                title: ''
            },
            tasks: {
                isLoading: false,
                list: []
            }
        })
        const dispatchedAction = { type: 'any action' }
        expect(tasksReducer(undefined, dispatchedAction)).toEqual(expectedInitialState)
    })
});

describe('Display Editor Dialog', () => {
    context('when showing the dialog', () => {
        it('should update editor coniguration state', () => {
            const initialState = fromJS({
                editorConfig: {
                    isEditMode: false,
                    isVisible: false
                }
            })
            const dispatchedAction = {
                type: DIALOG_EDIOR_TOGGLE_DISPLAY,
                payload: {
                    isEditMode: true,
                    isVisible: true
                }
            }

            const expectedState = fromJS({
                editorConfig: {
                    isEditMode: true,
                    isVisible: true
                }
            })
            expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
        })
    })
})

describe('New Task Details', () => {
    context('when updating the task title', () => {
        const NEW_TASK_TITLE = 'Task Title'

        it('should set the new task title', () => {
            const initialState = fromJS({ activeTask: { title: 'Task' } })
            const dispatchedAction = {
                type: SET_TASK_TITLE,
                payload: NEW_TASK_TITLE
            }

            const expectedState = fromJS({ activeTask: { title: NEW_TASK_TITLE } })
            expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
        })
    })

    context('when updating the task description', () => {
        const NEW_TASK_DESCRIPTION = 'Description'

        it('should set the new task description', () => {
            const initialState = fromJS({ activeTask: { description: 'Task' } })
            const dispatchedAction = {
                type: SET_TASK_DESCRIPTION,
                payload: NEW_TASK_DESCRIPTION
            }

            const expectedState = fromJS({ activeTask: { description: NEW_TASK_DESCRIPTION } })
            expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
        })
    })
})

describe('Task list', () => {
    context('Fetching the tasks list', () => {
        context('when requesting for the list', () => {
            it('should indicate that the data is being loaded', () => {
                const initialState = fromJS({ tasks: { isLoading: false } })
                const dispatchedAction = { type: GET_TASKS_LIST_REQUEST }

                const expectedState = fromJS({ tasks: { isLoading: true } })
                expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
            })

            it('should set the tasks list when the request is successful', () => {
                const TASKS_LIST = fromJS([{
                    title: 'Dune',
                    description: 'Fear is the mind killer'
                }])
                const initialState = fromJS({
                    tasks: {
                        isLoading: true,
                        list: [{
                            title: 'Watchmen',
                            description: 'Best comic ever!'
                        }]
                    }
                })
                const dispatchedAction = {
                    type: GET_TASKS_LIST_SUCCESS,
                    payload: TASKS_LIST
                }

                const expectedState = fromJS({
                    tasks: {
                        isLoading: false,
                        list: TASKS_LIST
                    }
                })
                expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
            })
        })
    })

    context('Creating a new task', () => {
        context('when creating a new task', () => {
            it('should indicate that the task is being processed by the server', () => {
                const initialState = fromJS({ isStoringTask: false })
                const dispatchedAction = { type: CREATE_TASK_REQUEST }

                const expectedState = fromJS({ isStoringTask: true })
                expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
            })

            it('should clear the active task data', () => {
                const initialState = fromJS({
                    activeTask: {
                        id: -1,
                        title: 'Dune',
                        description: 'Fear is the mind killer'
                    },
                    isStoringTask: true
                })
                const dispatchedAction = { type: CREATE_TASK_SUCCESS }

                const expectedState = fromJS({
                    activeTask: {
                        description: '',
                        id: undefined,
                        title: ''
                    },
                    isStoringTask: false
                })
                expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
            })
        })
    })
})

describe('Edit task', () => {
    context('when selecting a task', () => {
        it('should set the selected task as active', () => {
            const SELECTED_TASK = fromJS({
                description: 'The city is afraid of me',
                id: 321,
                title: 'Watchmen'
            })
            const initialState = fromJS({
                activeTask: {
                    description: '',
                    id: undefined,
                    title: ''
                }
            })
            const dispatchedAction = {
                type: SET_ACTIVE_TASK,
                payload: SELECTED_TASK
            }

            const expectedState = fromJS({ activeTask: SELECTED_TASK })
            expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
        })
    })

    context('when updating a task', () => {
        it('should indicate that the task is being processed by the server', () => {
            const initialState = fromJS({ isStoringTask: false })
            const dispatchedAction = { type: UPDATE_ACTIVE_TASK_REQUEST }

            const expectedState = fromJS({ isStoringTask: true })
            expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
        })

        it('should update the task in the list', () => {
            const UPDATED_LIST = [{
                description: 'Fear is the mind killer',
                id: 123,
                title: 'Dune'
            }]
            const initialState = fromJS({
                activeTask: {
                    id: -1,
                    title: 'Dune',
                    description: 'Fear is the mind killer'
                },
                isStoringTask: true
            })
            const dispatchedAction = {
                type: UPDATE_ACTIVE_TASK_SUCCESS,
                payload: UPDATED_LIST
            }

            const expectedState = fromJS({
                activeTask: {
                    description: '',
                    id: undefined,
                    title: ''
                },
                isStoringTask: false
            })
            expect(tasksReducer(initialState, dispatchedAction)).toEqual(expectedState)
        })
    })
})
