// @vendors
import React, { Component } from 'react'

// @components
import Tasks from './containers/tasks-container'

// @styles
import './style.scss'
import travixLogo from '../assets/images/travix-logo_blue.png'

class TodoManager extends Component {
    render() {
        return (
            <div className="todo-manager">
                <header className="todo-manager__header">
                    <img src={travixLogo} alt="Travix" className="todo-manager__title" />
                    <h2 className="todo-manager__subtitle">Task Manager</h2>
                </header>
                <div className="todo-manager__content">
                    <Tasks />
                </div>
            </div>
        )
    }
}

export default TodoManager
