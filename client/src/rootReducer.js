// @vendors
import { combineReducers } from 'redux'

// @components
import { tasksReducer } from './todo-manager/reducer'

export default combineReducers({ tasks: tasksReducer })
